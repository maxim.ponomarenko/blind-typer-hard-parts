# blind-typer-hard-parts

By practicing blind type for several years I still have problems and typos with 'border-keys' such as some of spec symbols sometimes numbers.
So in this repo I will create simple trainer for typing hard parts of blind typing. 

## Features done
[x] Implemented game logic
[x] User right input is immutable
[x] TypeString is centerd
[x] Sticky footer with Gmail, GitHub links

## In work
[ ] Buety Settings window
[ ] User can change game through settings