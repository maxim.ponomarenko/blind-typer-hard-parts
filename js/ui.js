"use strict";

class Settings {
  constructor() {
    //if settings don`t ever saved, give settings to a user
    if (!localStorage["settingsObject"]) {
      this.restoreDefaults();
    }
    //if there are local settings implement them to current game
    else {
      let parsedObj = JSON.parse(localStorage.getItem("settingsObject"));
      this.quizLength = parsedObj.quizLength;
      this.fontSize = parsedObj.fontSize;
      this.autoStart = parsedObj.autoStart;
    }
  }
  save() {
    localStorage.setItem("settingsObject", JSON.stringify(this));
  }
  restoreDefaults() {
    this.quizLength = 15;
    this.fontSize = 60;
    this.autoStart = false;
    this.save();
  }
}

/*Settings script implementations 
///////////////////////////////*/
//getting settingsBtn
const settingsBtn = document.getElementById("settingsBtn");
//getting closeSettingsBtn
const closeSettingsBtn = document.getElementById("closeSettingsBtn");
//getting settingsBox
const settingsBox = document.getElementById("settingsBox");
//save settings button
const saveSettingsBtn = document.getElementById("saveSettingsBtn");
//restore settings button
const restoreSettingsBtn = document.getElementById("restoreSettingsBtn");

settingsBtn.addEventListener("click", () => {
  if (settingsBox.style.display == "block") {
    settingsBox.style.display = "none";
  } else {
    settingsBox.style.display = "block";
  }
});

closeSettingsBtn.addEventListener("click", () => {
  settingsBox.style.display = "none";
});
