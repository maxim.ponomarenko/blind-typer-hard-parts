"use strict";

//symbolsArr contains border-symbols to train blind type: hard types
const symbolsArr = [
  "`",
  "!",
  "@",
  "#",
  "$",
  "%",
  "^",
  "&",
  "*",
  "(",
  ")",
  "~",
  "\\",
  "/",
  ",",
  ".",
  "<",
  ">",
  "?",
  '"',
  "'",
  "{",
  "}",
  "[",
  "]",
  "-",
  "=",
  ";",
  ":",
  "|",
];

//Vocabulare is a string with all latin letters and numbers 0-9 to add them to quiz string
const vocabulary = "abcdefghijklmnopqrstuvwxyz0123456789";

//This function replaces selected letter (in string) with a new one
function replaceLetter(str, letterIndex, newLetter) {
  str =
    str.substring(0, letterIndex) + newLetter + str.substring(letterIndex + 1);
  return str;
}

//This func inserts random letters / numbers from {vocabulary} (if needed)
function insertLetters(string, percentage = 25) {
  //insertCounter
  let insertCounter = Math.floor((string.length * percentage) / 100);
  for (let i = 0; i < insertCounter; i++) {
    let randomLetter =
      vocabulary[Math.floor(Math.random() * vocabulary.length)];
    string = replaceLetter(
      string,
      Math.floor(Math.random() * string.length),
      randomLetter
    );
  }

  return string;
}

//This func generates quiz string with chosen length
function stringGenerator(strLength) {
  let resultStr = ""; //variable to store output string
  //Creating quiz string with defined length
  for (let i = 0; i < strLength; i++) {
    resultStr += symbolsArr[Math.floor(Math.random() * symbolsArr.length)];
  }

  //this if statement will work with user settings
  if (true) {
    resultStr = insertLetters(resultStr);
  }

  return resultStr;
}
