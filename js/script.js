"use strict";

/* Getting UI elements */
//typeString - div element where will be displayed quiz string
const typeString = document.getElementById("typeString");
//userString - input:text element where user will input quiz string
const userString = document.getElementById("userString");
userString.focus(); // For better UX user string will be in focus (to input)
//Getting span to modify typed text in typeString
//const typedTextSpan = document.createElement("span");
//typedTextSpan.setAttribute("id", "typedText");
const typedText = document.getElementById("typedText");
const untypedText = document.getElementById("untypedText");

//Variable to store amount of letters in quiz string
let quizLength = 15;

//This func specifies every comand when quiz should start
function startQuiz() {
  typedText.innerText = "";
  untypedText.innerText = stringGenerator(quizLength);
}

window.addEventListener("keydown", (event) => {
  if (event.keyCode == 13) {
    userString.value = "";
    userString.focus();
    startQuiz();
  }
});

const liveStringHandler = function () {
  while (!typeString.innerText.startsWith(userString.value)) {
    if (userString.value.length != 1) {
      userString.value = userString.value.substring(
        0,
        userString.value.length - 1
      );
    } else {
      userString.value = "";
    }
  }
  //Adding opacity to typed text
  if (typedText.innerText != userString.value) {
    typedText.innerText = userString.value;
    untypedText.innerText = untypedText.innerText.slice(1);
  }
};

userString.addEventListener("keyup", (event) => {
  if (event.keyCode == 8 || event.keyCode == 46)
    userString.value = typedText.innerText;
  liveStringHandler();
});
